<?php

namespace App\Controller\Subscriber;


use App\Controller\BaseController\BaseController;
use App\Entity\Subscriber;
use App\Lib\Http\ResponseStatus;
use App\Lib\Subscriber\SubscriberField;
use App\Service\Subscriber\SubscriberService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriberController extends BaseController
{
    private $subscriberService;

    public function __construct(SubscriberService $subscriberService)
    {
        $this->subscriberService = $subscriberService;
    }

    /**
     * @Route("/subscribe", methods={"POST", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function subscribe(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $subscriber = $this->prepareSubscribeData($data);
        $this->subscriberService->subscribe($subscriber);

        return $this->json([
            "status" => ResponseStatus::CREATED
        ]);
    }


    /**
     * @Route("/unsubscribe", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function unsubscribe(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        $subscriber = $this->prepareSubscribeData($data);
        $this->subscriberService->unsubscribe($subscriber);

        return new JsonResponse(null, Response::HTTP_OK);
    }

    /**
     * @Route("/following/{id}", methods={"GET", "HEAD"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getSubscriptions($id)
    {
        $subscriptions = $this->subscriberService->getSubscriptions((int)$id);

        return $this->json([
            "status" => ResponseStatus::CREATED,
            "users" => $subscriptions
        ]);
    }


    /**
     * @Route("/followers/{id}", methods={"GET", "HEAD"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getFollowers($id)
    {
        $followers = $this->subscriberService->getFollowers((int)$id);

        return $this->json([
            "status" => ResponseStatus::CREATED,
            "users" => $followers
        ]);
    }

    /**
     * @param array $data
     * @return Subscriber
     * @throws \Exception
     */
    public function prepareSubscribeData(array $data): Subscriber
    {
        $followerId = $this->tryToGetJsonData($data, SubscriberField::FOLLOWER);
        $followingId = $this->tryToGetJsonData($data, SubscriberField::FOLLOWING);

        $subscriber = new Subscriber();
        $subscriber->setFollowerId($followerId);
        $subscriber->setFollowingId($followingId);

        return $subscriber;
    }
}
