<?php

namespace App\Controller\NewsFeed;

use App\Controller\BaseController\BaseController;
use App\Service\NewsFeedService\NewsFeedService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsFeedController extends BaseController
{
    private $feedService;

    public function __construct(NewsFeedService $feedService)
    {
        $this->feedService = $feedService;
    }

    /**
     * @Route("/feed", methods={"GET", "HEAD"})
     */
    public function getNewsFeed()
    {
        $user = $this->getLoggedUser();

        $feed = $this->feedService->getFeed($user);

        return new JsonResponse($feed, Response::HTTP_OK);
    }
}
