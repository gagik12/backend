<?php

namespace App\Controller\BaseController;

use App\Entity\Account;
use Couchbase\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    protected function getLoggedUser(): ?Account
    {
        return $this->getUser();
    }

    protected function isUserAuth()
    {
        return !is_null($this->getLoggedUser());
    }

    /**
     * @param array $data
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    protected function tryToGetJsonData(array $data, string $key)
    {
        if (!array_key_exists($key, $data))
        {
            throw new \Exception("Key not exist in array " . $key);
        }

        return $data[$key];
    }

    /**
     * @param array $data
     * @param string $key
     * @return mixed
     */
    protected function isDataExist(array $data, string $key): bool
    {
        if (!array_key_exists($key, $data))
        {
            return false;
        }

        return true;
    }
}
