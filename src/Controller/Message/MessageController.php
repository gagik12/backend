<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 14.04.19
 * Time: 14:09
 */

namespace App\Controller\Message;


use App\Controller\BaseController\BaseController;
use App\Entity\Message;
use App\Lib\Http\ResponseStatus;
use App\Lib\Message\MessageField;
use App\Service\Message\MessageService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends BaseController
{

    private $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * @Route("/message", methods={"POST", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function postMessage(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $message = $this->prepareMessageData($data);
        $this->messageService->saveMessage($message);

        return new JsonResponse(Response::HTTP_OK);
    }

    /**
     * @Route("/messages", methods={"GET", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getMessages(Request $request)
    {
        $receiver_id = $request->query->get("sender_id");
        $sender_id = $request->query->get("receiver_id");

        $messages =$this->messageService->getMessages($sender_id, $receiver_id);

        return new JsonResponse(['messages' => $messages],Response::HTTP_OK);
    }

    /**
     * @param array $data
     * @return Message
     * @throws \Exception
     */
    public function prepareMessageData(array $data): Message
    {
        $data = $this->tryToGetJsonData($data, MessageField::DATA);
        $receiverId = $this->tryToGetJsonData($data, MessageField::RECEIVER_ID);
        $senderId = $this->tryToGetJsonData($data, MessageField::SENDER_ID);

        $message = new Message();
        $message->setData($data);
        $message->setReceiverId($receiverId);
        $message->setSenderId($senderId);

        return $message;
    }

}
