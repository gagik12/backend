<?php

namespace App\Controller\Account;

use App\DTO\Account\AccountInfoDTO;
use App\Entity\Account;
use App\Service\Account\AccountService;
use App\Service\Post\PostService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\BaseController\BaseController;

class AccountController extends BaseController
{
    private $accountService;
    private $postService;

    public function __construct(AccountService $accountService, PostService $postService)
    {
        $this->accountService = $accountService;
        $this->postService = $postService;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/AccountController.php',
        ]);
    }

    /**
     * @Route("/account/{id}", methods={"GET","HEAD"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function accountInfoAction($id)
    {
        $account = $this->accountService->getAccountById($id);

        if (is_null($account))
        {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return $this->json([
            "info" => $account->serialize(),
            "posts" => $this->postService->getAllAccountPosts($account)
        ]);
    }

    /**
     * @Route("/update", methods={"POST", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function updateAccount(Request $request) {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $account = $this->prepareAccountData($data);

        $this->accountService->updateAccount($account);

        return new JsonResponse(Response::HTTP_OK);
    }

    /**
     * @param $data
     * @return Account
     * @throws \Exception
     */
    public function prepareAccountData($data): Account
    {
        $id = $this->tryToGetJsonData($data, "id");
        $type = $this->tryToGetJsonData($data, "type");
        $password = $this->tryToGetJsonData($data, "password");
        $name = $this->tryToGetJsonData($data, "name");
        $email = $this->tryToGetJsonData($data, "email");
        $image = $this->tryToGetJsonData($data, "image");

        $account = new Account();
        $account->setType($id);
        $account->setType($type);
        $account->setPassword($password);
        $account->setName($name);
        $account->setEmail($email);
        $account->setImage($image);

        return $account;
    }
}
