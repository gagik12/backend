<?php

namespace App\Controller\Account;

use App\Lib\Utils\ImageUtils;
use App\Lib\Admin\AdminType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Account;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Lib\Http\ResponseStatus;
use App\Controller\BaseController\BaseController;
use App\Lib\Decoder\Base64Decoder;

define("DEFAULT_FOR_BANNED", 0);

class AccountRegistrationController extends BaseController
{
    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        if ($this->isUserAuth())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        try 
        {
            $em = $this->getDoctrine()->getManager();
            $account = $this->prepareAccountByRequest($request, $encoder);
            $em->persist($account);
            $em->flush();
        } 
        catch (UniqueConstraintViolationException $exception)
        {
            return  new JsonResponse(null, Response::HTTP_CONFLICT);
        }
        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Account
     * @throws \Exception
     */
    protected function prepareAccountByRequest(Request $request, UserPasswordEncoderInterface $encoder): Account
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $email = $this->tryToGetJsonData($data, "email");
        $password = $this->tryToGetJsonData($data, "password");
        $type = (int) $this->tryToGetJsonData($data, "type");
        $name = $this->tryToGetJsonData($data, "name");
        //$imageData = $this->tryToGetJsonData($data, "account_image");
        $account = new Account($email);
//        if ($imageData)
//        {
//            $image = $this->prepareImage($imageData);
//            $em = $this->getDoctrine()->getRepository(Image::class);
//            $em->saveImage($image);
//            $account->setImage($image);
//        }
        $account->setName($name);
        $account->setType($type);
        $account->setPassword($encoder->encodePassword($account, $password));
        $account->setBanned(DEFAULT_FOR_BANNED);
        $account->setIsAdmin(AdminType::USER);
        return $account;
    }

//    private function prepareImage(string $imageData): Image
//    {
//        $imagePath = ImageUtils::loadImage($imageData, "uploads/account_image/");
//        $image = new Image();
//        $image->setImagePath($imagePath);
//        return $image;
//    }
}
