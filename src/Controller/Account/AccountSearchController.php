<?php
namespace App\Controller\Account;


use App\Controller\BaseController\BaseController;
use App\Entity\Account;
use App\Service\Account\AccountService;
use Symfony\Component\HttpFoundation\Request;

class AccountSearchController extends BaseController
{
    private $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getAccounts(Request $request)
    {
        $search_str = $request->query->get('search_str');
        return $this->json([
            "accounts" => $this->accountService->findAccounts($search_str)
        ]);
    }
}
