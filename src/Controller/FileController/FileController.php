<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 01.04.19
 * Time: 1:58
 */

namespace App\Controller\FileController;


use App\Controller\BaseController\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\BinaryFileResponse;

class FileController extends BaseController
{
    /**
     * @Route("/image", methods={"GET", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function getImage(Request $request)
    {
        $imagePath = $request->query->get('image_path');

        return new BinaryFileResponse($imagePath);
    }
}
