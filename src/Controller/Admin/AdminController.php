<?php


namespace App\Controller\Admin;

use App\Controller\BaseController\BaseController;
use App\Entity\Account;
use App\Entity\Post;
use App\Lib\Post\PostField;
use App\Service\Admin\AdminService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Account\AccountService;
use App\Service\Post\PostService;

class AdminController extends BaseController
{
    private $accountService;
    private $postService;
    private $adminService;

    public function __construct(AccountService $accountService, PostService $postService, AdminService $adminService)
    {
        $this->accountService = $accountService;
        $this->postService = $postService;
        $this->adminService = $adminService;
    }


    /**
     * @Route("/admin/users", methods={"GET","HEAD"})
     * @return JsonResponse
     */
    public function getAllAccount()
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $accounts = $this->accountService->getAllAccounts();
        return new JsonResponse($accounts, Response::HTTP_OK);
    }

    /**
     * @Route("/admin/users/ban", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function banAccount(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }

        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        $account = $this->prepareAccountData($data);
        if (!is_null($account))
        {
            $this->adminService->banAccount($account);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Нет такого пользователя"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/admin/users/unban", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function unbanAccount(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        $account = $this->prepareAccountData($data);
        if (!is_null($account))
        {
            $this->adminService->unbanAccount($account);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Нет такого пользователя"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/admin/users/delete", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAccount(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        $account = $this->prepareAccountData($data);
        if (!is_null($account))
        {
            $this->adminService->deleteAccount($account);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Нет такого пользователя"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/admin/post/ban", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function banPost(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );
        $post = $this->preparePostData($data);
        if (!is_null($post))
        {
            $this->adminService->banPost($post);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Пост не найден"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/admin/post/unban", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function unbanPost(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );
        $post = $this->preparePostData($data);
        if (!is_null($post))
        {
            $this->adminService->unbanPost($post);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Пост не найден"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/admin/post/delete", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deletePost(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        if (!$loggedUser->isAdmin())
        {
            return new JsonResponse(null, Response::HTTP_FORBIDDEN);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );
        $post = $this->preparePostData($data);
        if (!is_null($post))
        {
            $this->adminService->deletePost($post);
            return new JsonResponse(null, Response::HTTP_OK);
        }
        $message = [
            "message" => "Пост не найден"
        ];
        return new JsonResponse($message, Response::HTTP_NOT_FOUND);
    }

    public function prepareAccountData($data): ?Account
    {
        $accountId = $this->tryToGetJsonData($data, "id");
        $account = $this->accountService->getAccountById($accountId);
        return $account;
    }

    public function preparePostData($data): ?Post
    {
        $postId = $postId = $this->tryToGetJsonData($data, PostField::ID);
        $post = $this->postService->getPostById($postId);
        return $post;
    }
}
