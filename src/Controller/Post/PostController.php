<?php
namespace App\Controller\Post;


use App\Controller\BaseController\BaseController;
use App\Lib\Post\PostField;
use App\Lib\Post\PostType;
use App\Service\Account\AccountService;
use App\Service\Post\PostService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends BaseController
{
    private $postService;
    private $accountService;

    public function __construct(PostService $postService, AccountService $accountService)
    {
        $this->postService = $postService;
        $this->accountService = $accountService;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function createPostAction(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }
        $data = json_decode(
            $request->getContent(),
            true
        );
        $postType = $this->tryToGetJsonData($data, PostField::TYPE);
        if ($postType == PostType::EXHIBIT)
        {
            $this->postService->tryToSaveExhibitPost($data, $loggedUser);
        }
        else if ($postType == PostType::EXPOSURE)
        {
            $this->postService->tryToSaveExposurePost($data, $loggedUser);
        }
        return new JsonResponse(null, Response::HTTP_CREATED);
    }

    /**
     * @Route("/delete/post", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletePostAction(Request $request)
    {
        $loggedUser = $this->getLoggedUser();
        if (is_null($loggedUser))
        {
            return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
        }

        $data = json_decode(
            $request->getContent(),
            true
        );

        $postId = $this->tryToGetJsonData($data, PostField::ID);

        if (is_null($postId))
        {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $isDeleted = $this->postService->deletePost($postId, $loggedUser);

        if ($isDeleted)
        {
            return new JsonResponse(null, Response::HTTP_OK);
        }

        return new JsonResponse(null, Response::HTTP_FORBIDDEN);
    }

    /**
     * @Route("/exhibit_posts/account/{id}", methods={"GET","HEAD"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAccountExhibitPosts($id)
    {
        $account = $this->accountService->getAccountById($id);

        if (is_null($account))
        {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        return $this->json($this->postService->getAllAccountPosts($account, true));
    }
}
