<?php

namespace App\Controller\Post;

use App\Controller\BaseController\BaseController;
use App\Entity\Like;
use App\Lib\Like\LikeField;
use App\Service\Post\LikeService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends BaseController
{

    private $likeService;

    public function __construct(LikeService $likeService)
    {
        $this->likeService = $likeService;
    }

    /**
     * @Route("/like", methods={"POST", "HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function like(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $like = $this->prepareSubscribeData($data);

        $likeResult = $this->likeService->addLike($like);

        return new JsonResponse(['like' => $likeResult], Response::HTTP_CREATED);
    }


    /**
     * @Route("/unlike", methods={"POST", "HEAD"})
     */
    public function unlike(Request $request)
    {
        $data = json_decode(
            $request->getContent(),
            true
        );

        $unlike = $this->prepareSubscribeData($data);

        $unlikeResult = $this->likeService->deleteLike($unlike);

        return new JsonResponse(['like' => $unlikeResult], Response::HTTP_OK);
    }

    public function prepareSubscribeData(array $data): Like
    {
        $userId = $this->tryToGetJsonData($data, LikeField::USER_ID);
        $postId = $this->tryToGetJsonData($data, LikeField::POST_ID);

        $like = new Like();
        $like->setUserId($userId);
        $like->setPostId($postId);
        return $like;
    }
}
