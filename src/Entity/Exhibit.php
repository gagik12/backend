<?php

namespace App\Entity;

use App\Lib\Post\PostType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExhibitRepository")
 */
class Exhibit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExposureItem", mappedBy="exposure")
     */
    private $exposureItems;

    public function __construct()
    {
        $this->exposureItems = new ArrayCollection();
    }

    /**
     * @return Collection|ExposureItem[]
     */
    public function getExposureItems()
    {
        return $this->exposureItems;
    }

    public function setImage(Image $image)
    {
        $this->image = $image;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        $imagePath = $this->image ? $this->image->getImagePath() : null;
        return array(
            //"image_path" => $imagePath,
            //"video" => $this->video,
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "imageSrc" => $imagePath,
            "videoSrc" => $this->video,
            "type" => PostType::EXHIBIT,
        );
    }
}
