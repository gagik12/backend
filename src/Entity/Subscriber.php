<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriberRepository")
 */
class Subscriber
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\Column(type="integer")
     */
    private $follower_id;

    /**
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\Column(type="integer")
     */
    private $following_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFollowerId(): ?int
    {
        return $this->follower_id;
    }

    public function getFollowingId(): ?int
    {
        return $this->following_id;
    }

    public function setFollowerId(int $followerId): self
    {
        $this->follower_id = $followerId;
        return $this;
    }

    public function setFollowingId(int $followingId): self
    {
        $this->following_id = $followingId;
        return $this;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return array(
            "id" => $this->id,
            "follower_id" => $this->follower_id,
            "following_id" => $this->following_id
        );
    }
}
