<?php

namespace App\Entity;

use App\Lib\Post\PostType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExposureRepository")
 */
class Exposure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ExposureItem", mappedBy="exposure")
     */
    private $exposureItems;

    /**
     * @ORM\Column(type="integer")
     */
    private $display_type;

    public function __construct()
    {
        $this->exposureItems = new ArrayCollection();
    }

    /**
     * @return Collection|ExposureItem[]
     */
    public function getExposureItems()
    {
        return $this->exposureItems;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDisplayType(): ?int
    {
        return $this->display_type;
    }

    public function setDisplayType(int $display_type): self
    {
        $this->display_type = $display_type;

        return $this;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        $result = [
            "id" => $this->id,
            "title" => $this->title,
            "description" => $this->description,
            "type" => PostType::EXPOSURE,
            "display_type" => $this->display_type,
            "exposure_item_count" => count($this->exposureItems)
        ];

        $exhibits = [];
        foreach ($this->exposureItems as $exposureItem)
        {
            $exhibits[] = $exposureItem->serialize();
        }
        $result["exhibits"] = $exhibits;

        return $result;
    }
}
