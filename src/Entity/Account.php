<?php
namespace App\Entity;

use App\Lib\Admin\AdminType;
use App\Lib\Account\AccountType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="account")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="account")
     */
    private $posts;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $banned;

    /**
     * @ORM\Column(type="integer")
     */
    private $is_admin;

    public function __construct(string $email)
    {
        $this->email = $email;
        $this->posts = new ArrayCollection();
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getAccountId(): int
    {
        return $this->id;
    }

    public function setImage(Image $image)
    {
        $this->image = $image;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }


    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getType():int
    {
        return $this->type;
    }

    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getRoles(): array
    {
        return array(AccountType::convertToString($this->type, "en"));
    }

    public function eraseCredentials()
    {
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getBanned(): ?int
    {
        return $this->banned;
    }

    public function setBanned(int $banned): self
    {
        $this->banned = $banned;

        return $this;
    }

    /**
     * @return array
     */
    public function serialize()
    {
        $imagePath = $this->image ? $this->image->getImagePath() : null;
        return [
            //"image_path" => $imagePath,
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "type" => $this->type,
            "avatarSrc" => $imagePath,
            "banned" => $this->banned
        ];
    }
    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAccount($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getAccount() === $this) {
                $post->setAccount(null);
            }
        }

        return $this;
    }

    public function getIsAdmin(): ?int
    {
        return $this->is_admin;
    }

    public function setIsAdmin(int $is_admin): self
    {
        $this->is_admin = $is_admin;

        return $this;
    }

    public function isAdmin(): bool
    {
        if ($this->is_admin === AdminType::ADMIN)
        {
            return true;
        }
        return false;
    }
}
