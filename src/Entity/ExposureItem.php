<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExposureItemRepository")
 */
class ExposureItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exposure", inversedBy="exposureItem")
     * @ORM\JoinColumn(nullable=true)
     */
    private $exposure;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exhibit", inversedBy="exposureItem")
     * @ORM\JoinColumn(nullable=true)
     */
    private $exhibit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExposureId(): Exposure
    {
        return $this->exposure;
    }

    public function setExposure(Exposure $exposure)
    {
        $this->exposure = $exposure;
    }

    public function getExhibit(): Exhibit
    {
        return $this->exhibit;
    }

    public function setExhibit(Exhibit $exhibit)
    {
        $this->exhibit = $exhibit;
    }

    public function serialize(): array
    {
        return $this->exhibit->serialize();
    }
}
