<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Account;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image_path;

    /**
     * @ORM\OneToMany(targetEntity="Account", mappedBy="image")
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity="Exhibit", mappedBy="image")
     */
    private $exhibit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImagePath(): ?string
    {
        return $this->image_path;
    }

    public function setImagePath(string $image_path): self
    {
        $this->image_path = $image_path;

        return $this;
    }
}
