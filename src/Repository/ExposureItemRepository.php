<?php

namespace App\Repository;

use App\Entity\ExposureItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExposureItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExposureItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExposureItem[]    findAll()
 * @method ExposureItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExposureItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExposureItem::class);
    }

    public function save(ExposureItem $exposureItem)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($exposureItem);
        $entityManager->flush();
    }

    // /**
    //  * @return ExposureItem[] Returns an array of ExposureItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExposureItem
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
