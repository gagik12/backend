<?php

namespace App\Repository;

use App\Entity\Like;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Like|null find($id, $lockMode = null, $lockVersion = null)
 * @method Like|null findOneBy(array $criteria, array $orderBy = null)
 * @method Like[]    findAll()
 * @method Like[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LikeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Like::class);
    }


    public function getCountByPostId($post_id)
    {
        return $this->createQueryBuilder('l')
            ->select('count(l.post_id)')
            ->andWhere('l.post_id = :post_id')
            ->setParameter('post_id', $post_id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getUserIdsByPostId($post_id)
    {
        $usersIds = $this->createQueryBuilder('l')
            ->select('l.user_id')
            ->andWhere('l.post_id = :post_id')
            ->setParameter('post_id', $post_id)
            ->getQuery()
            ->getResult();

        return array_column($usersIds, 'user_id');
    }

    public function save(Like $like)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($like);
        $entityManager->flush();
    }

    public function delete(Like $unlike)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($unlike);
        $entityManager->flush();
    }

    // /**
    //  * @return Like[] Returns an array of Like objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Like
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
