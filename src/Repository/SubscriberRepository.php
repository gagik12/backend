<?php

namespace App\Repository;

use App\Entity\Subscriber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Subscriber|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subscriber|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subscriber[]    findAll()
 * @method Subscriber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriberRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subscriber::class);
    }

    public function save(Subscriber $subscriber)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($subscriber);
        $entityManager->flush();
    }

    public function delete(Subscriber $subscriber)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($subscriber);
        $entityManager->flush();
    }

    public function getFollowingId($id)
    {
        $followingIds = $this->createQueryBuilder('s')
            ->select('s.following_id')
            ->andWhere('s.follower_id = :follower_id')
            ->setParameter('follower_id', $id)
            ->getQuery()
            ->getArrayResult();

        return array_column($followingIds, 'following_id');
    }
}
