<?php

namespace App\Repository;

use App\Entity\Exposure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Exposure|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exposure|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exposure[]    findAll()
 * @method Exposure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExposureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Exposure::class);
    }

    public function save(Exposure $exposure)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($exposure);
        $entityManager->flush();
    }

    // /**
    //  * @return Exposure[] Returns an array of Exposure objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exposure
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
