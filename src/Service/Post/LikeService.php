<?php


namespace App\Service\Post;


use App\Entity\Like;
use App\Service\Base\BaseService;
use Doctrine\ORM\EntityManagerInterface;

class LikeService extends BaseService
{
    private $likeRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->likeRepository = $entityManager->getRepository(Like::class);
    }

    public function addLike(Like $like): array
    {
        $isExist = $this->likeRepository->findOneBy([
                'user_id' => $like->getUserId(),
                'post_id' => $like->getPostId()
            ]);

        if (empty($isExist))
        {
            $this->likeRepository->save($like);
        }

        $result = [
            'count' => $this->likeRepository->getCountByPostId($like->getPostId()),
            'users' => $this->likeRepository->getUserIdsByPostId($like->getPostId())
        ];

        return $result;
    }

    public function deleteLike(Like $unlike): array
    {
        $unlike = $this->likeRepository->findOneBy([
            'user_id' => $unlike->getUserId(),
            'post_id' => $unlike->getPostId()
        ]);

        if (!empty($unlike))
        {
            $this->likeRepository->delete($unlike);
        }

        $result = [
            'count' => $this->likeRepository->getCountByPostId($unlike->getPostId()),
            'users' => $this->likeRepository->getUserIdsByPostId($unlike->getPostId())
        ];

        return $result;
    }
}