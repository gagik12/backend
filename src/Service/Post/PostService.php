<?php


namespace App\Service\Post;


use App\Entity\Account;
use App\Entity\Exhibit;
use App\Entity\Exposure;
use App\Entity\ExposureItem;
use App\Entity\Image;
use App\Entity\Like;
use App\Lib\Admin\BanType;
use App\Entity\Post;
use App\Lib\Post\PostField;
use App\Lib\Post\PostType;
use App\Lib\Utils\ImageUtils;
use App\Service\Base\BaseService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class PostService extends BaseService
{
    private $postRepository;
    private $exhibitRepository;
    private $likeRepository;
    private $imageRepository;
    private $exposureRepository;
    private $exposureItemRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->postRepository = $this->entityManager->getRepository(Post::class);
        $this->exhibitRepository = $this->entityManager->getRepository(Exhibit::class);
        $this->imageRepository = $this->entityManager->getRepository(Image::class);
        $this->likeRepository = $this->entityManager->getRepository(Like::class);
        $this->exposureRepository = $this->entityManager->getRepository(Exposure::class);
        $this->exposureItemRepository = $this->entityManager->getRepository(ExposureItem::class);
    }

    public function getPostById($id)
    {
        return $this->postRepository->find((int)$id);
    }

    /**
     * @param Account $account
     * @return array
     * @throws \Exception
     */
    public function getAllAccountPosts(Account $account, bool $returnOnlyExhibitPosts = false)
    {
        $postsInfo = [];
        $posts = $this->postRepository->getAllPostsByAccount($account);

        /** @var Post $post */
        foreach ($posts as $post)
        {
            $info = [];
            if ($post->getType() == PostType::EXHIBIT)
            {
                /** @var Exhibit $exhibit */
                $exhibit = $this->exhibitRepository->getExhibitById($post->getContentId());

                if ($exhibit === null)
                {
                    continue;
                }
                if (!$returnOnlyExhibitPosts)
                {
                    $info['content'] = $exhibit->serialize();
                }
                else
                {
                    $info = $exhibit->serialize();
                }
            }
            else if ($post->getType() == PostType::EXPOSURE)
            {
                /** @var Exposure $exposure */
                $exposure = $this->exposureRepository->find($post->getContentId());

                if ($returnOnlyExhibitPosts || ($exposure === null))
                {
                    continue;
                }
                $info['content'] = $exposure->serialize();
            }
            if (!$returnOnlyExhibitPosts)
            {
                $this->setAdditionalPostInfo($info, $post, $account);
            }
            $postsInfo[] = $info;
        }

        return $postsInfo;
    }

    private function setAdditionalPostInfo(array &$info, Post $post, Account $account)
    {
        $info['id'] = $post->getId();
        $timestamp = $post->getTimestamp();
        $dt = new DateTime("@$timestamp");
        $info['timestamp'] = $dt->format('Y-m-d H:i:s');
        $info['banned'] = $post->getBanned();
        $info['like'] = [
            'likeCount' => $this->likeRepository->getCountByPostId($post->getId()),
            'users' => $this->likeRepository->getUserIdsByPostId($post->getId())
        ];
        $info['author'] = [
            'userId' => $account->getAccountId(),
            'name' => $account->getName(),
            'avatarSrc' => $account->getImage()
        ];
    }

    public function deletePost(int $postId, Account $loggedUser): bool
    {
        $post = $this->postRepository->findOneBy([
            'id' => $postId,
            'account' => $loggedUser
        ]);

        if (!is_null($post))
        {
            $this->postRepository->delete($post);
            return true;
        }

        return false;
    }



    /**
     * @param array $data
     * @param Account $loggedUser
     * @throws \Exception
     */
    public function tryToSaveExhibitPost(array $data, Account $loggedUser)
    {
        $exhibit = $this->prepareExhibit($data);
        $imageData = $this->tryToGetJsonData($data, PostField::IMAGE);
        if ($imageData)
        {
            $image = $this->prepareImage($imageData);
            $this->imageRepository->saveImage($image);
            $exhibit->setImage($image);
        }

        $em = $this->exhibitRepository;
        $em->save($exhibit);

        $post = $this->preparePost(PostType::EXHIBIT, $exhibit->getId(), $loggedUser);
        $em = $this->postRepository;
        $em->save($post);
    }

    /**
     * @param array $data
     * @param Account $loggedUser
     */
    public function tryToSaveExposurePost(array $data, Account $loggedUser)
    {
        $exposure = $this->saveExposure($data);

        $post = $this->preparePost(PostType::EXPOSURE, $exposure->getId(), $loggedUser);
        $this->postRepository->save($post);
    }

    /**
     * @param array $data
     * @return Exposure
     * @throws \Exception
     */
    private function saveExposure(array $data): Exposure
    {
        $title = $this->tryToGetJsonData($data, PostField::TITLE);
        $description = $this->tryToGetJsonData($data, PostField::DESCRIPTION);
        $exhibitItems = $this->tryToGetJsonData($data, PostField::EXHIBIT_IDS);
        $displayType = $this->tryToGetJsonData($data, PostField::DISPLAY_TYPE);

        $exposure = new Exposure();
        $exposure->setTitle($title);
        $exposure->setDescription($description);
        $exposure->setDisplayType($displayType);

        $this->exposureRepository->save($exposure);

        foreach ($exhibitItems as $exhibitItem)
        {
            $exposureItem = new ExposureItem();
            $exposureItem->setExhibit($this->exhibitRepository->find($exhibitItem["id"]));
            $exposureItem->setExposure($exposure);
            $this->exposureItemRepository->save($exposureItem);
        }

        return $exposure;
    }

    /**
     * @param array $data
     * @return Exhibit|null
     * @throws \Exception
     */
    private function prepareExhibit(array $data): Exhibit
    {
        $title = $this->tryToGetJsonData($data, PostField::TITLE);
        $description = $this->tryToGetJsonData($data, PostField::DESCRIPTION);
        $video = $this->tryToGetJsonData($data, PostField::VIDEO);

        $exhibit = new Exhibit();
        $exhibit->setTitle($title);
        $exhibit->setDescription($description);
        $exhibit->setVideo($video);

        return $exhibit;
    }

    /**
     * @param string $imageData
     * @return Image
     * @throws \Exception
     */
    private function prepareImage(string $imageData): Image
    {
        $imagePath = ImageUtils::loadImage($imageData, "uploads/exhibits_image/");
        $image = new Image();
        $image->setImagePath($imagePath);
        return $image;
    }

    /**
     * @param int $type
     * @param int $contentId
     * @param Account $loggedUser
     * @return Post
     */
    private function preparePost(int $type, int $contentId, Account $loggedUser): Post
    {
        $post = new Post();
        $post->setAccount($loggedUser);
        $post->setType($type);
        $post->setContentId($contentId);
        $post->setTimestamp(time());
        $post->setBanned(BanType::UNBAN);

        return $post;
    }

    /**
     * @param array $data
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    private function tryToGetJsonData(array $data, string $key)
    {
        if (!array_key_exists($key, $data))
        {
            throw new \Exception("Key not exist in array " . $key);
        }

        return $data[$key];
    }
}
