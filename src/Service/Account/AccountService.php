<?php

namespace App\Service\Account;

use App\Entity\Account;
use App\Service\Base\BaseService;

class AccountService extends BaseService
{
    /**
     * @param int $id
     * @return null|Account
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAccountById(int $id): ?Account
    {
        $repository = $this->entityManager->getRepository(Account::class);
        $account = $repository->getAccountById($id);
        return $account;
    }

    /**
     * @param string $searchStr
     * @return array
     */
    public function findAccounts(string $searchStr)
    {
        $postRepository = $this->entityManager->getRepository(Account::class);
        $accounts = $postRepository->getAccountsBySearchStr($searchStr);

        return $this->serializeEntities($accounts);
    }

    public function getAllAccounts()
    {
        $repository = $this->entityManager->getRepository(Account::class);
        $accounts = $repository->findAll();
        return $this->serializeEntities($accounts);
    }

    public function updateAccount(Account $account)
    {
        $updatedAccount = $this->getAccountById($account->getAccountId());
        $this->setAccountFields($updatedAccount, $account);

        $this->entityManager->flush();
    }

    private function setAccountFields(Account $updatedAccount, Account $account)
    {
        $type = $account->getType();
        $password = $account->getPassword();
        $name = $account->getName();
        $email = $account->getEmail();
        $image = $account->getImage();

        if ($type != "") $updatedAccount->setType($type);
        if ($password != "") $updatedAccount->setPassword($password);
        if ($name != "") $updatedAccount->setName($name);
        if ($email != "") $updatedAccount->setEmail($email);
        if ($image != "") $updatedAccount->setImage($image);
    }
}
