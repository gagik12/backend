<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 14.04.19
 * Time: 14:14
 */

namespace App\Service\Message;


use App\Entity\Message;
use App\Service\Base\BaseService;
use Doctrine\ORM\EntityManagerInterface;

class MessageService extends BaseService
{
    private $messageRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->messageRepository = $entityManager->getRepository(Message::class);
    }

    public function saveMessage(Message $message)
    {
        $this->messageRepository->save($message);
    }

    public function getMessages($sender_id, $receiver_id)
    {
        return $this->messageRepository->findBy(
            ['sender_id' => $sender_id],
            ['receiver_id' => $receiver_id]
        );
    }
}
