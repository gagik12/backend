<?php


namespace App\Service\Admin;


use App\Entity\Account;
use App\Entity\Post;
use App\Service\Base\BaseService;
use App\Lib\Admin;

class AdminService extends BaseService
{
    public function banAccount(Account $account)
    {
        $account->setBanned(Admin\BanType::BAN);
        $this->entityManager->flush();
    }

    public function unbanAccount(Account $account)
    {
        $account->setBanned(Admin\BanType::UNBAN);
        $this->entityManager->flush();
    }

    public function deleteAccount(Account $account)
    {
        $accountRepository = $this->entityManager->getRepository(Account::class);
        $accountRepository->delete($account);
    }


    public function banPost(Post $post)
    {
        $post->setBanned(Admin\BanType::BAN);
        $this->entityManager->flush();
    }


    public function unbanPost(Post $post)
    {
        $post->setBanned(Admin\BanType::UNBAN);
        $this->entityManager->flush();
    }


    public function deletePost(Post $post)
    {
        $postRepository = $this->entityManager->getRepository(Post::class);
        $postRepository->delete($post);
    }
}
