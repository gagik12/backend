<?php


namespace App\Service\NewsFeedService;


use App\Entity\Account;
use App\Entity\Exhibit;
use App\Entity\Exposure;
use App\Entity\ExposureItem;
use App\Entity\Like;
use App\Entity\Post;
use App\Entity\Subscriber;
use App\Lib\Post\PostType;
use App\Service\Base\BaseService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class NewsFeedService extends BaseService
{
    private $subscriberRepository;
    private $accountRepository;
    private $postRepository;
    private $likeRepository;
    private $exhibitRepository;
    private $exposureRepository;
    private $exposureItemRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->subscriberRepository = $entityManager->getRepository(Subscriber::class);
        $this->accountRepository = $entityManager->getRepository(Account::class);
        $this->postRepository = $entityManager->getRepository(Post::class);
        $this->likeRepository = $entityManager->getRepository(Like::class);
        $this->exhibitRepository = $entityManager->getRepository(Exhibit::class);
        $this->exposureRepository = $this->entityManager->getRepository(Exposure::class);
        $this->exposureItemRepository = $this->entityManager->getRepository(ExposureItem::class);
    }

    public function getFeed(Account $user, bool $returnOnlyExhibitPosts = false)
    {
        $followingIds = $this->subscriberRepository->getFollowingId($user->getAccountId());

        $posts = $this->postRepository->getAllPostsByAccountIds($followingIds);

        $postsInfo = [];

        for ($i = 0; $i < count($posts); ++$i)
        {
            $info = [];
            if ($posts[$i]->getType() == PostType::EXHIBIT)
            {
                /** @var Exhibit $exhibit */
                $exhibit = $this->exhibitRepository->getExhibitById($posts[$i]->getContentId());

                if ($exhibit === null)
                {
                    continue;
                }
                if (!$returnOnlyExhibitPosts)
                {
                    $info['content'] = $exhibit->serialize();
                }
                else
                {
                    $info = $exhibit->serialize();
                }
            }
            else if ($posts[$i]->getType() == PostType::EXPOSURE)
            {
                /** @var Exposure $exposure */
                $exposure = $this->exposureRepository->find($posts[$i]->getContentId());

                if ($returnOnlyExhibitPosts || ($exposure === null))
                {
                    continue;
                }
                $info['content'] = $exposure->serialize();
            }
            if (!$returnOnlyExhibitPosts)
            {
                $author = $posts[$i]->getAccount();
                $this->setAdditionalPostInfo($info, $posts[$i], $author);
            }
            $postsInfo[] = $info;
        }

        return $postsInfo;
    }

    private function setAdditionalPostInfo(array &$info, Post $post, Account $account)
    {
        $info['id'] = $post->getId();
        $timestamp = $post->getTimestamp();
        $dt = new DateTime("@$timestamp");
        $info['timestamp'] = $dt->format('Y-m-d H:i:s');
        $info['banned'] = $post->getBanned();
        $info['like'] = [
            'likeCount' => $this->likeRepository->getCountByPostId($post->getId()),
            'users' => $this->likeRepository->getUserIdsByPostId($post->getId())
        ];
        $info['author'] = [
            'userId' => $account->getAccountId(),
            'name' => $account->getName(),
            'avatarSrc' => $account->getImage()
        ];
    }
}
