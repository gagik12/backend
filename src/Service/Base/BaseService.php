<?php
namespace App\Service\Base;


use Doctrine\ORM\EntityManagerInterface;

class BaseService
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function serializeEntities(array $entities)
    {
        $data = [];

        foreach ($entities as $entitie)
        {
            $data[] = $entitie->serialize();
        }

        return $data;
    }
}