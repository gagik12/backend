<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 31.03.19
 * Time: 12:20
 */

namespace App\Service\Subscriber;


use App\Entity\Subscriber;
use App\Service\Account\AccountService;
use App\Service\Base\BaseService;
use Doctrine\ORM\EntityManagerInterface;

class SubscriberService extends BaseService
{
    private $subscriberRepository;
    private $accountService;
    public function __construct(EntityManagerInterface $entityManager, AccountService $accountService)
    {
        parent::__construct($entityManager);
        $this->subscriberRepository = $entityManager->getRepository(Subscriber::class);
        $this->accountService = $accountService;
    }

    public function subscribe(Subscriber $subscriber)
    {
        $isExist = $this->subscriberRepository->findOneBy(
            array('following_id' => $subscriber->getFollowingId(),
                  'follower_id' => $subscriber->getFollowerId())
        );
        if (empty($isExist))
        {
            $this->subscriberRepository->save($subscriber);
        }
    }

    public function unsubscribe(Subscriber $subscriber)
    {
        $subscription = $this->subscriberRepository->findOneBy(
          [
              'following_id' => $subscriber->getFollowingId(),
              'follower_id'  => $subscriber->getFollowerId()
          ]
        );

        if (!empty($subscription))
        {
            $this->subscriberRepository->delete($subscription);
        }
    }

    public function getSubscriptions($id)
    {
        $subscriptions = $this->subscriberRepository->findBy(
            ['follower_id' => $id]
        );

        return $this->serializeSubscriptions($subscriptions);
    }

    public function getFollowers($id)
    {
        $followers = $this->subscriberRepository->findBy(
            ['following_id' => $id]
        );

        return $this->serializeFollowers($followers);
    }

    private function serializeSubscriptions($subscriptions)
    {
        $accounts = [];

        for ($i = 0; $i < count($subscriptions); ++$i)
        {
            $sub = $subscriptions[$i];

            $accounts[$i] = $this->accountService->getAccountById($sub->getFollowingId())->serialize();
        }

        return $accounts;
    }

    private function serializeFollowers($followers)
    {
        $accounts = [];

        for ($i = 0; $i < count($followers); ++$i)
        {
            $follower = $followers[$i];

            $accounts[$i] = $this->accountService->getAccountById($follower->getFollowerId())->serialize();
        }

        return $accounts;
    }
}
