<?php
namespace App\Lib\Post;


class PostField
{
    public const ID = "id";
    public const TITLE = "title";
    public const DESCRIPTION = "description";
    public const VIDEO = "videoSrc";
    public const IMAGE = "imageSrc";
    public const TYPE = "type";
    public const EXHIBIT_IDS = "exhibit_ids";
    public const DISPLAY_TYPE = "display_type";
    //public const VIDEO = "link_to_video";
    //public const IMAGE = "image";
}
