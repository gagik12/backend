<?php
namespace App\Lib\Post;


class PostType
{
    public const EXHIBIT = 1;
    public const EXPOSURE = 2;
}