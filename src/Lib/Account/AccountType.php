<?php
namespace App\Lib\Account;


class AccountType
{
    public const COLLECTOR = 10;
    public const MUSEUM = 20;
    public const MODERATOR = 30;
    public const OWNER = 40;
    private const ALL_TYPE  = [
        self::COLLECTOR => [
            "ru" => "Коллекционер",
            "en" => "Collector",
        ],
        self::MUSEUM => [
            "ru" => "Музей",
            "en" => "Museum",
        ],
        self::MODERATOR => [
            "ru" => "Модератор",
            "en" => "Moderator",
        ],
        self::OWNER => [
            "ru" => "Владелец",
            "en" => "Owner",
        ]
    ];

    public static function convertToString(int $userRole, string $lang): string
    {
        return self::ALL_TYPE[$userRole][$lang];
    }
}