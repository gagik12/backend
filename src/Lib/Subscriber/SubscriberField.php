<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 31.03.19
 * Time: 12:41
 */

namespace App\Lib\Subscriber;


class SubscriberField
{
    public const FOLLOWER = "follower_id";
    public const FOLLOWING = "following_id";
}
