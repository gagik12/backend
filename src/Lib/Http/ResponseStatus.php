<?php
namespace App\Lib\Http;


class ResponseStatus
{
    public const CREATED = 201;
    public const OK = 200;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const FORBIDDEN = 403;
    public const NOT_FOUND = 404;
    public const CONFLICT = 409;
}