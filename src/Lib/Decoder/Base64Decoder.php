<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 11.03.19
 * Time: 0:18
 */

namespace App\Lib\Decoder;


class Base64Decoder
{
    static function imageDecode(string $data)
    {
        $image = null;
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type))
        {
            $data = substr($data, strpos($data, ',') + 1);
            $data = str_replace(' ', '+', $data);
            $type = strtolower($type[1]);

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }

            $image = base64_decode($data);

            if ($data === false)
            {
                throw new \Exception('base64_decode failed');
            }
        } else
        {
            throw new \Exception('did not match data URI with image data');
        }
        return $image;
    }

    static function getImageType(string $data)
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type))
        {
            $type = strtolower($type[1]);
        }
        else
        {
            throw new \Exception('did not match data URI with image data');
        }
        return $type;
    }
}