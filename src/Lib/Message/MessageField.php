<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 20.04.19
 * Time: 13:51
 */

namespace App\Lib\Message;


class MessageField
{
    public const DATA = "data";
    public const RECEIVER_ID = "receiver_id";
    public const SENDER_ID = "sender_id";
}
