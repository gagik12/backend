<?php
namespace App\Lib\Utils;

use App\Lib\Decoder\Base64Decoder;

class ImageUtils
{
    public static function loadImage(string $imageData, string $dir)
    {
        $accountImage = Base64Decoder::imageDecode($imageData);
        $currentDate = date('m-d-Y_his');
        $filePath = $dir . "image" . $currentDate . "." . Base64Decoder::getImageType($imageData);
        file_put_contents($filePath, $accountImage);

        return $filePath;
    }
}