<?php
namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $account = $event->getUser();

        if (!$account instanceof UserInterface) {
            return;
        }

        $data['userId'] = $account->getAccountId();
        $data['banned'] = $account->getBanned();
        $data['admin'] = $account->isAdmin();

        $event->setData($data);
    }
}
